<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\Item;

class ClaimController extends Controller
{
    public function index(){
        $items = Item::with('claim')->get();

        return view('claim', compact('items'));
    }
    
    public function claim_reward(Request $request){
        $claim = Claim::where('mail', $request->mail )->with('reward')->firstOrFail();
        
        if($claim->item_id == null){
            $item = Item::where('is_claimed', 0)->inRandomOrder()->first();
            if(!$item){
                return redirect()->back();
            }
            $item->is_claimed = 1;
            $item->save();
            $claim->item_id = $item->id;
            $claim->save();
        }
        
        
        return redirect()->route('result', $claim->id);
    }
    
    public function result(Claim $claim){
        // dd($claim);
        return view('result', compact('claim') );
    }
    
}
