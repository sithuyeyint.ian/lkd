<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    protected $fillable = ['name', 'item_id'];

    public function reward(){
        return $this->belongsTo('App\Item', 'item_id');
    }
}
