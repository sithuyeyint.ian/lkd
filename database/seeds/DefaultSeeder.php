<?php

use Illuminate\Database\Seeder;
use App\Claim;
use App\Item;

class DefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $claims = ['zaw@myansis.com',
                   'thinnwesu@myansis.com',
                   'suhtweaung@myansis.com',
                   'sithuyeyint@myansis.com',
                   'htetnyinyiaung@myansis.com',
                   'heinhtetaung@myansis.com',
                   'kyawyeaung@myansis.com',
                   'sulattphyu@myansis.com',
                   'kokokyaw@myansis.com',
                   'wailinnaung@myansis.com',
                   'naychi@myansis.com',
                   'khaingzarchi@myansis.com;',
                   'daewichan@myansis.com',
                   'yupar@myansis.com',
                   'aint@myansis.com',
                   'nanglaung@myansis.com',
                   'kyawzinthet@myansis.com',
                   'miminge@myansis.com',
                   'myothantkyaw@myansis.com',
                   'zinmaraung@myansis.com',
                   'zarzar@myansis.com',
                   'yinmyatmin@myansis.com',
                   'aungzawhtet@atgsys.com',
                   'aye.cho@mm.lantro.com',
                   'wy.cho@mm.lantro.com',
                   'thandar60@gmail.com',
                   'myominlin@myansis.com',
                   'dawkhinmyathet@gmail.com',
                    ];

        $items = [ 
            1 =>	'Pillow(YouType)',
            2 =>	'Remax earphone(RM-711)',
            3 =>	'Remax earphone(RM-512)',
            4 =>	'Shirt (light-blue)',
            5 =>	'Shirt (white)',
            6 =>	'Shirt (grey)',
            7 =>	'Shirt (black)',
            8 =>	'Women Slegend(Brush)',
            9 =>	'Make-up (Brush)',
            10 =>	'Water Bottle',
            11 =>	'Vaseline Men',
            12 =>	'Pond Men',
            13 =>	'Pond flawless white',
            14 =>	'Pond acne clear',
            15 =>	'Galanz',
            16 =>	'Gatsby water gell',
            17 =>	'BeNice(Mystic White)',
            18 =>	'BeNice(Beautiful Whitening)',
            19 =>	'Lux(Soft Rose)',
            20 =>	'Lux(Magiacal Spell)',
            21 =>	'Earphone(RM-201)',
            22 =>	'Earphone(RM-202)',
            23 =>	'Earphone(RM-208)',
            24 =>	'Earphone(RM-550)',
            25 =>	'Girl Accessories',
            26 =>	'Nail Clipper and comb tooth',
            27 =>	'Nail Clipper',
            28 =>	'Lux(80ml)'
            
        ];
        
        foreach ($claims as $key => $value) {
            Claim::create(['mail' => $value]);
        }                    
        
        foreach ($items as $key => $value) {
            Item::create(['id' => $key,
                          'name' => $value]);
        }                    
    }
}
